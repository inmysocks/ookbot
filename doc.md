Simple Zulip bot that will respond to it's name or in a rumors channel.

If anyone says the bots name in a channel anywhere in their statement
it will say either 'Someone said {botname}!' or say something from the
set of nice things.

If someone says anything in a channel called Rumors it will tell a
rumor about that person.

If you say both the bots name and 'eddie izzard' (case insensitive)
than it will reply with an eddie izzard quote.
