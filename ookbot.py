# See readme.md for instructions on running this code.

"""
To do:

1 - make a voting mechanism using the bot
2 - add a command to tell the bot to listen or stop listening to a stream
3 - add a command to add quotes, rumors and nice things to the list
4 - add a greeting message to new people
"""

from typing import Any, Dict
from random import randint
import zulip
from random import random, randint
import re
import os

f = open('{}/EddieIzzardQuotes.txt'.format(os.path.dirname(os.path.realpath(__file__))), 'r')
eddie_izzard_quotes = f.readlines()
f.close()

f = open('{}/NiceThings.txt'.format(os.path.dirname(os.path.realpath(__file__))), 'r')
nice_things = f.readlines()
f.close()

f = open('{}/Rumors.txt'.format(os.path.dirname(os.path.realpath(__file__))), 'r')
rumors = f.readlines()
f.close()

f = open('{}/Streams.txt'.format(os.path.dirname(os.path.realpath(__file__))), 'r')
stream_list = f.readlines()
stream_list = [stream[:-1] for stream in stream_list]
f.close()

helpString = (
    "Hello! This is the help for {0}, an interactive bot for this server.\n"
    "I do the following things:\n"
    "* Quote Eddie Izzard if you say both '{0}' and 'eddie izzard' in the same message.\n"
    "* If you say anything in the Rumors stream I will tell a salacious rumor about you.\n"
    "* If you start a message with '{0} please say ', I will say anything you put after it.\n"
    "* If you say '{0}' and 'help' in the same message that I will send you a private message with this help info.\n"
    "* You can add a rumor by saying '{0} please add rumor ' followed by a rumor it will be added to the list. Add {1} in it to place a persons name."
    "* You can add a complement by saying '{0} please add nice thing ' followed by a nice thing to add it to the list. Put {1} in it to include a persons name."
    "* If you say '{0}' in a message that doesn't match any of the other options I will sometimes respond with something nice.\n\n"
    "**Notes:** I only respond if you are talking in a stream that I am listening to and everything is case-insensitive, so capitalisation doesn't matter."
    "Also you have to say please or I will ignore you."
)

botname = re.compile(r'(.+)?{}'.format(os.getenv("ZULIP_BOT_NAME", "Bob")),re.I)

pleaseexp = re.compile(r'(.+)?please', re.I)

listexp = re.compile(r'list',re.I)

regedd = re.compile(r'(.+)?eddie\ izzard',re.I)

helpregex = re.compile(r'(.+)?help', re.I)

botsay = re.compile(r'^{}\ please\ say (.+)'.format(os.getenv("ZULIP_BOT_NAME", "Bob")), re.I)

addrumor = re.compile(r'^{}\ please\ add\ rumor\ '.format(os.getenv("ZULIP_BOT_NAME", "Bob")), re.I)

addnicething = re.compile(r'^{}\ please\ add\ nice\ thing\ '.format(os.getenv("ZULIP_BOT_NAME", "Bob")), re.I)

class OokBotHandler(object):
    def usage(self) -> str:
        return '''
        If anyone says the bots name in a channel anywhere in their statement
        it will say either 'Someone said {botname}!' or say something from the
        set of nice things.

        If someone says anything in a channel called Rumors it will tell a
        rumor about that person.

        If you say both the bots name and 'eddie izzard' (case insensitive)
        than it will reply with an eddie izzard quote.
        '''

    def initialize(self, bot_handler: Any) -> None:
        def process(self):
            if not (self["sender_short_name"].endswith("-bot") or self["client"] == "ZulipPython") and stream_list.count(self["display_recipient"]) > 0:
                if self["subject"] == "Rumors":
                    ind = randint(0,len(rumors)-1)
                    other.send_message({
                        "type": "stream",
                        "subject": "Rumors",
                        "content": "/me says: " + rumors[ind].format(self["sender_full_name"]),
                        "to": self["display_recipient"]
                    })
                elif botname.match(self["content"]) and pleaseexp.match(self["content"]):
                    if botsay.match(self["content"]):
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "/me says: " + self["content"][len('{} please say '.format(os.getenv("ZULIP_BOT_NAME", "Bob"))):],
                            "to": self["display_recipient"]
                        })
                    elif helpregex.match(self["content"]):
                        other.send_message({
                            "type": "private",
                            "subject": self["subject"],
                            "content": helpString.format(os.getenv("ZULIP_BOT_NAME", "Bob"), '{}'),
                            "to": self["sender_email"]
                        })
                    elif regedd.match(self["content"]):
                        ind = randint(0,len(eddie_izzard_quotes)-1)
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "As Eddie Izzard once said,\n>" + eddie_izzard_quotes[ind],
                            "to": self["display_recipient"]
                        })
                    elif addrumor.match(self["content"]):
                        newrumor = self["content"][len('{} please add rumor '.format(os.getenv("ZULIP_BOT_NAME", "Bob"))):]
                        if not newrumor.startswith("*psst!!* "):
                            newrumor = "*psst!!* " + newrumor
                        newrumor = '"' + newrumor + '"'
                        with open('Rumors.txt', 'a') as f:
                            f.writelines(newrumor)
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "Ok, added a new rumor",
                            "to": self["display_recipient"]
                        })
                    elif addnicething.match(self["content"]):
                        newnicething = '"' + self["content"][len('{} please add nice thing '.format(os.getenv("ZULIP_BOT_NAME", "Bob"))):] + '"'
                        with open('NiceThings.txt', 'a') as f:
                            f.writelines(newnicething)
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "Ok, added a new nice thing",
                            "to": self["display_recipient"]
                        })
                    elif listexp.match(self["content"]):
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "I am listening to these channels:\n{}".format('\n'.join(stream_list)),
                            "to": self["display_recipient"]
                        })
                    elif random() < 0.75:
                        ind = randint(0,len(nice_things)-1)
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "/me says " + nice_things[ind].format(self["sender_full_name"]),
                            "to": self["display_recipient"]
                        })
                    else:
                        other.send_message({
                            "type": "stream",
                            "subject": self["subject"],
                            "content": "Someone said {}!!".format(os.getenv("ZULIP_BOT_NAME", "Bob")),
                            "to": self["display_recipient"]
                        })
                elif botname.match(self["content"]):
                    other.send_message({
                        "type": "stream",
                        "subject": self["subject"],
                        "content": "Someone said {}!!".format(os.getenv("ZULIP_BOT_NAME", "Bob")),
                        "to": self["display_recipient"]
                    })
        other = zulip.Client(os.environ.get("ZULIP_EMAIL"), os.environ.get("ZULIP_API_KEY"), None, False, True, os.environ.get("ZULIP_SITE"))
        #json = other.get_streams()["streams"]
        #streams = [{"name":stream["name"]} for stream in json]
        streams = [{"name": stream} for stream in stream_list]
        other.add_subscriptions(streams)
        other.call_on_each_message(process)

    def handle_message(self, message: Dict[str, Any], bot_handler: Any) -> None:
        ind = randint(0,len(eddie_izzard_quotes)-1)
        if message['content'] == 'eddie izzard':
            content = eddie_izzard_quotes[ind]  # type: str
        if message['content'].startswith('vote '):
            content = 'VOTE??'
        bot_handler.send_reply(message, content)

handler_class = OokBotHandler
